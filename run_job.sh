#!/bin/bash

# exit if docker command is not available
!(type -P "docker" &>/dev/null || type -P "docker.exe" &>/dev/null) && echo "docker not found" && exit 1

# check if job name is provided
if [ -z "$1" ]; then
  echo "you should provide job name"
  exit 1
fi;

# fixes issues of docker in windows git bash
if [ "$OSTYPE" = "msys" ]; then
  docker()
  {
          (export MSYS_NO_PATHCONV=1; "docker.exe" "$@")
  }
fi

# if "debug" is set, pass it to gitlab-runner
if [ ! -z "$debug" ]
then
  debug_runner="--debug --log-level debug"
fi

# get project name from git command
project_name=$(git config --local remote.origin.url|sed -n 's#.*/\([^.]*\)\.git#\1#p')
# project directory in container
project_parent_dir="/tmp/gitlab-runner-tmp"
project_dir="$project_parent_dir/$project_name"

### I have no idea why the container the runner creates uses the host file system instead of the gitlab-runner
### so I make it available here then mount it to container
rm -rf "$project_parent_dir";
mkdir -p "$project_parent_dir";
cp -a . "$project_dir";
chmod -R 777 "$project_parent_dir";

# generate usable yaml file
docker run --rm -i -v $project_dir:/gitlab-ci-processor javadmnjd/gitlab-ci-processor
# replace generated file with original one
rm -rf "$project_dir/.gitlab-ci.yml"
mv "$project_dir/.gitlab-ci-all.yml" "$project_dir/.gitlab-ci.yml"

# set runner environment variables
env_args_array=(
  "CI_PROJECT_TITLE=$project_name"
  "CI_PROJECT_NAME=$project_name"
);

# make the statement
env_args=""
for var in "${env_args_array[@]}"
do
  env_args="$env_args --env $var"
done

# eliminate the need for double slash in mounting the socket on windows, I guess
export COMPOSE_CONVERT_WINDOWS_PATHS=1;
# give gitlab-runner image access to docker daemon
docker_volumes="-v /var/run/docker.sock:/var/run/docker.sock";
# and a volume to pass between jobs
docker_volumes="$docker_volumes -v $project_parent_dir:$project_parent_dir"
# override entrypoint to use our custom commands
docker_entrypoint="--entrypoint /bin/bash";
# gitlab-runner image (use the one you prefer)
# runner_image="gitlab/gitlab-runner"
runner_image="gitlab/gitlab-runner:alpine";
# combine docker args
docker_cmd_flags="$docker_volumes $docker_entrypoint $runner_image"

# creates a container and returns it's id
create_job() {
  local job_name=$1;
  docker_command="docker create $docker_cmd_flags \
    -c ' \
      chmod -R 777 $project_dir;
      cd $project_dir;
      gitlab-runner $debug_runner exec docker
        --clone-url \"file://$project_dir\"
        $env_args 
        $job_name; 
    '";

  local container_id=$(eval $docker_command);
  # echo container id
  echo $container_id;
}

# create gitlab runner
dcid=$(create_job $1);
# starts the container and wait for it to finish using -i
docker start -i $dcid;
job_result=$?;
# remove the container
docker rm $dcid;

red_esc="\e[1;31m"
green_esc="\e[1;32m"
reset_esc="\e[0m"

if [ "$job_result" -eq 0 ]; then
  echo -e "$green_esc"
  echo -e "############################\n#### Job succeeded\n############################"
  echo -e "$reset_esc"
else
  echo -e "$red_esc"
  echo -e "############################\n#### Job failed: exit code $job_result\n############################"
  echo -e "$reset_esc"
fi;
