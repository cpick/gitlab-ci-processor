require "test/unit/assertions"
include Test::Unit::Assertions

require "zeitwerk"
loader = Zeitwerk::Loader.new
loader.setup

# run main.rb
require "../main.rb"

outputPath = "./.gitlab-ci-all.yml"

fileIsGenerated = File.exist?(outputPath)
assert_equal(fileIsGenerated, true)
puts "Generation passed"

genFileContent = File.read(outputPath)
fileContainsIncludedJobs = genFileContent.include? "extended"
assert_equal(fileContainsIncludedJobs, true)
puts "Content check passed"
