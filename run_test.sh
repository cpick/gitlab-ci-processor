set -e

# fixes issue of WSL if you have ruby on windows
use_win_ruby() {
  echo "using ruby.exe from windows env"
  ruby()
  {
    ("ruby.exe" "$@")
  }
}
type -P "ruby.exe" &>/dev/null && use_win_ruby

cd ./test/
ruby test.rb
